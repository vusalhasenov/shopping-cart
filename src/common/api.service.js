import axios from "axios";

export function get(path=''){
    return axios.get(path).catch(err=>{
        console.log(err);
        throw Error("Http Error: Api Service")
    })
}